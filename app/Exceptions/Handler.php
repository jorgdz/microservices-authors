<?php

namespace App\Exceptions;

use Throwable;
use App\Traits\RestResponse;
use Illuminate\Http\Response;
use App\Exceptions\Custom\ConflictException;
use Illuminate\Auth\AuthenticationException;
use App\Exceptions\Custom\BadRequestException;
use Illuminate\Validation\ValidationException;
use App\Exceptions\Custom\UnprocessableException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    use RestResponse;

    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        if ($exception instanceof NotFoundHttpException) {
            $code = $exception->getStatusCode();
            return $this->error($request->getPathInfo(), $exception, __('messages.not-found'), $code);
        }

        if ($exception instanceof HttpException) {
            $code = $exception->getStatusCode();
            return $this->error($request->getPathInfo(), $exception, __('messages.method-not-allowed'), $code);
        }

        if ($exception instanceof ModelNotFoundException) {
            $model = strtolower(class_basename($exception->getModel()));
            return $this->error($request->getPathInfo(), $exception,
                __('messages.no-exist-instance', ['model' => $model]), Response::HTTP_NOT_FOUND);
        }

        if ($exception instanceof AuthorizationException) {
            return $this->error($request->getPathInfo(), $exception,
                __('messages.firbidden'), Response::HTTP_FORBIDDEN);
        }

        if ($exception instanceof AuthenticationException) {
            return $this->error($request->getPathInfo(), $exception,
                __('messages.no-authorize'), Response::HTTP_UNAUTHORIZED);
        }

        if ($exception instanceof ValidationException) {
            $errors = $exception->validator->errors()->getMessages();
            return $this->error($request->getPathInfo(), $exception,
                $errors, Response::HTTP_BAD_REQUEST);
        }

        if ($exception instanceof UnprocessableException
            || $exception instanceof ConflictException
            || $exception instanceof BadRequestException
            ) {

            $code = $exception->getStatusCode();
            $message = $exception->getMessage();
            return $this->error($request->getPathInfo(), $exception, $message, $code);
        }

        if (env('APP_DEBUG')) {
            //return parent::render($request, $exception);
            return $this
                ->error($request->getPathInfo(), $exception, $exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->error($request->getPathInfo(), $exception, __('messages.internal-server-error'), Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
