<?php

namespace App\Http\Controllers;

use App\Models\Author;
use App\Traits\RestResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Exceptions\Custom\UnprocessableException;

class AuthorController extends Controller
{
    use RestResponse;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Return author list
     * @return Illuminate\Http\Response
     */
    public function index () {
        $authors = Author::orderBy('id', 'desc')
            ->paginate(10);

        return $this->success($authors);
    }

    /**
     * Create an instance of author
     * @return Illuminate\Http\Response
     */
    public function store (Request $request) {
        $rules = [
            'name' => 'required|max:255',
            'gender' => 'required|max:255|in:male,female',
            'country' => 'required|max:255'
        ];

        $this->validate($request, $rules);

        $author = Author::create($request->all());

        return $this->success($author, Response::HTTP_CREATED);
    }

    /**
     * Return a one author
     * @return Illuminate\Http\Response
     */
    public function show ($author) {
        $author = Author::findOrFail($author);
        return $this->success($author);
    }

    /**
     * Update the information of an existing author
     * @return Illuminate\Http\Response
     */
    public function update (Request $request, $author) {
        $rules = [
            'name' => 'max:255',
            'gender' => 'max:255|in:male,female',
            'country' => 'max:255'
        ];

        $this->validate($request, $rules);

        $author = Author::findOrFail($author);

        $author->fill($request->all());

        if ($author->isClean())
            throw new UnprocessableException(__('messages.nochange'));

        $author->save();

        return $this->success($author);
    }

    /**
     * Removes an existing author
     * @return Illuminate\Http\Response
     */
    public function destroy ($author) {
        $author = Author::findOrFail($author);

        $author->delete();

        return $this->success($author);
    }
}
